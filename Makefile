TARGETS = jargonswe.html

all: $(TARGETS)

.SUFFIXES: .html .md

.md.html:
	gm --gm-typographer=false -c style.css $<

clean:
	rm $(TARGETS)
