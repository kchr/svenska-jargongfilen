# Svenska jargongfilen

Ett lexikon över svensk hackerslang och uppslagsbok för svensk
hackerdom.

Se [jargonswe.md](jargonswe.md).

För att bygga lokalt, kör `make`. Kräver
[gm](https://github.com/kpym/gm), en tunn fernissa ovanpå Goldmark som
stöder CommonMark.

## Äldre versioner

Under [archive](archive) finns äldre versioner av jargongfilen i lite
olika format. I kronologisk ordning:

- [Hacker-uttryck på KTH (NADA och Elektro), LiTH samt
  Uppsala](archive/hackerswe.txt) utan år men troligen 80-tal.
  Troligen först publicerad på papper i
  [Stackens](https://stacken.kth.se/) tidning [StackPointer 2, 1981
  sidan
  49](http://elvira.stacken.kth.se/~mz/sp/stackpointer-1981-2.pdf).
  
- [Svenska Hackademins Ordlista v4.0c](archive/shol4.0c.html) från
  1996.

- [Svenska jargongfilen 2003-05-05
  (13)](archive/texinfo/hackerswe.html). Källkod i TeXinfo och
  Info-versioner under [texinfo](archive/texinfo). Underhölls från
  90-talets mitt till 2003.

## Redigering

- Markera eventuellt uttal med \/uttal\/.
- Markera eventuellt ursprung före förklaring, till exempel: [Lysator].
- Skriv ut förkortningar.
- Håll raderna i Markdown-filen under 72 tecken.
- Små bokstäver på uppslagsorden om det inte är egennamn eller
  förkortningar.
